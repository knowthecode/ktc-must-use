<?php

/**
 * User's History Last Viewed Shortcode
 *
 * @package     Library\Shortcode
 * @since       1.2.1
 * @author      hellofromTonya
 * @link        https://UpTechLabs.io
 * @license     GNU-2.0+
 */
namespace User_History\Shortcode;

class LastViewed extends UserHistoryBase {

	/**
	 * Build the Shortcode HTML and then return it.
	 *
	 * NOTE: This is the method to extend for enhanced shortcodes (i.e. which extend this class).
	 *
	 * @since 1.2.1
	 *
	 * @return string Shortcode HTML
	 */
	protected function render() {

		if ( is_admin() ) {
			return '';
		}

		$post_id   = $this->get_last_viewed();
		$post      = $post_id ? get_post( $post_id ) : false;
		$permalink = $post ? get_permalink( $post_id ) : '';

		ob_start();

		include( $this->config->view );

		return ob_get_clean();
	}

	/**************
	 * Helpers
	 *************/

	/**
	 * Builds the arguments for the query.
	 *
	 * @since 1.2.0
	 *
	 * @return array
	 */
	protected function get_last_viewed() {
		return (int) get_user_meta( get_current_user_id(), '_ktc_last_viewed', true );
	}
}
