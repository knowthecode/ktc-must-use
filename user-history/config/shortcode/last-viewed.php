<?php

/**
 * Last Viewed - Runtime Configuration Parameters
 *
 * @package     Library\Shortcode
 * @since       1.2.0
 * @author      hellofromTonya
 * @link        https://UpTechLabs.io
 * @license     GNU General Public License 2.0+
 */

namespace User_History\Shortcode;

return array(
	'autoload'  => true,
	'classname' => 'User_History\Shortcode\LastViewed',
	'config'    => array(
		'shortcode' => 'last_viewed',
		'view'      => USER_HISTORY_PLUGIN_DIR . 'src/shortcode/views/last-viewed.php',
		'item_view'     => array(
			'default'      => USER_HISTORY_PLUGIN_DIR . 'src/shortcode/views/user-history-item.php',
			'lab'          => USER_HISTORY_PLUGIN_DIR . 'src/shortcode/views/user-history-lab.php',
			'embedded_lab' => USER_HISTORY_PLUGIN_DIR . 'src/shortcode/views/user-history-embedded-lab.php',
		),
		'defaults'  => array(
			'class'      => '',
			'post_types' => array( 'post', 'lab', 'docx' ),
		),
	),
);
